<?php

use Illuminate\Database\Seeder;

class AddUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' =>'myname',
                 'email' =>'a@a.com',
                 'password' => Hash::make('12345678'),
                 'created_at' => date('Y-m-d G:i:s'),
                 'role'=>'admin',
            ],
 
                 ]);
     }
    }

