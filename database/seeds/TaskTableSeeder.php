<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('tasks')->insert([
           [
                'title' => 'task 1',
                'user_id' =>1,
                'created_at' => date('Y-m-d G:i:s'),
                'status' =>0,
           ],

           [
            'title' => 'task 2',
            'user_id' =>1,
            'created_at' => date('Y-m-d G:i:s'),
            'status' =>0,
            ],
            [
                'title' => 'task 3',
                'user_id' =>1,
                'created_at' => date('Y-m-d G:i:s'),
                'status' =>0,
            ],     
                ]);
    }
}
