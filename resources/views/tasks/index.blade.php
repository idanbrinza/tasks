@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<h1>This is the task list</h1>
@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>

@endif


<ul>
@foreach($tasks as $task)
<li>
    <a style ="padding-right:5%" href = "{{route('tasks.edit',$task->id)}}">{{$task->title}} </a>

    @can('admin')
    @if ($task->status)
           <button id ="{{$task->id}}" value="1"> Done!</button>
       @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif

<form method = 'POST' style="width:5%" action ="{{action('TaskController@destroy', $task->id)}}">

@csrf
@method('DELETE')
<div class = "form-group">
<input style="color:red" type = "submit" class= "form-control" name = "submit" value = "Delete">
</div>

</form>
@endcan
</li>

@endforeach
</ul>
<a href = "{{route('tasks.create')}}">Add a new Todo </a>
<br><br>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>
@endsection
