<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;


        /*        if (Auth::guest()) {
            abort(403,"You have no permission to enter");
        }*/

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = Task::All();
        return view('tasks.index', ['tasks' => $tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /* if (Gate::denies('manager')) {
        abort(403,"Sorry you are not allowed to create todos..");
    }
*/
        return view ('tasks.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     /*   if (Gate::denies('manager')) {
             abort(403,"Are you a hacker or what?");
        }*/
        $task = new Task();
        $id = Auth::id(); //the id of the current user
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status = 0;
        $task->save();
        return redirect('tasks');  
     }
  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
/*        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }*/
        $task = Task::find($id);
    return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
   {
       //only if this todo belongs to user
       $task = Task::find($id);
      //employees are not allowed to change the title 
   /*    if (Gate::denies('manager')) {
           if ($request->has('title'))
                  abort(403,"You are not allowed to edit todos..");
       }   
   */
       //make sure the todo belongs to the logged in user
  //     if(!$task->user->id == Auth::id()) return(redirect('tasks'));
  
       //test if title is dirty
      
       $task->update($request->except(['_token']));
  
      if($request->ajax()){
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to check tasks");
       }
           return Response::json(array('result' => 'success1','status' => $request->status ), 200);
       } else {          
           return redirect('tasks');           
       }
   }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"You are not allowed to delete tasks");
       }
        $task = Task::find($id);
        $task->delete();
        return redirect('tasks');
    }

    public function mytasks()
    { 
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = $user->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
    }
}
